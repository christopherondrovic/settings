# HOW-TO


This assumes your are using docker and the Dockerfile is in the root of the project. If this is not the case you will have to adjust the settings around the following commands

```
    - docker build . --file Dockerfile --tag ${IMAGE_NAME}
```